#!/bin/bash

# hc-kernel-assemble kernel-bitcode kernel-object

# tools search priority:
# 1) $HCC_HOME
# 2) @CMAKE_INSTALL_PREFIX@ : default install directory
# 3) @LLVM_TOOLS_DIR@ : build directory

# enable bash debugging
KMDBSCRIPT="${KMDBSCRIPT:=0}"

# dump the LLVM bitcode
KMDUMPLLVM="${KMDUMPLLVM:=0}"

if [ $KMDBSCRIPT == "1" ]
then
  set -x
fi

if [ -n "$HCC_HOME" ] && [ -e "$HCC_HOME" ]; then
  BINPATH=$HCC_HOME/bin
  CLANG=$BINPATH/clang
  CLAMP_CONFIG=$BINPATH/clamp-config
  LLVM_LINK=$BINPATH/llvm-link
  CLAMP_ASM=$BINPATH/clamp-assemble
  OPT=$BINPATH/opt
  LLVM_AS=$BINPATH/llvm-as
  LLVM_DIS=$BINPATH/llvm-dis
  LIBPATH=$HCC_HOME/lib
elif [ -e @CMAKE_INSTALL_PREFIX@ ]; then
  BINPATH=@CMAKE_INSTALL_PREFIX@/bin
  CLANG=$BINPATH/clang
  CLAMP_CONFIG=$BINPATH/clamp-config
  LLVM_LINK=$BINPATH/llvm-link
  CLAMP_ASM=$BINPATH/clamp-assemble
  OPT=$BINPATH/opt
  LLVM_AS=$BINPATH/llvm-as
  LLVM_DIS=$BINPATH/llvm-dis
  LIBPATH=@CMAKE_INSTALL_PREFIX@/lib
elif [ -d @LLVM_TOOLS_DIR@ ]; then
  BINPATH=@LLVM_ROOT@/bin
  CLANG=$BINPATH/clang
  CLAMP_CONFIG=@EXECUTABLE_OUTPUT_PATH@/clamp-config
  LLVM_LINK=$BINPATH/llvm-link
  CLAMP_ASM=$BINPATH/clamp-assemble
  OPT=$BINPATH/opt
  LLVM_AS=$BINPATH/llvm-as
  LLVM_DIS=$BINPATH/llvm-dis
  LIBPATH=@LLVM_ROOT@/lib
else
    echo "ERROR: Can NOT locate HCC tools! Please specify with $HCC_HOME environmental variable." >&2
    exit 1
fi

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 kernel-bitcode object" >&2
  exit 1
fi

if [ ! -f $1 ]; then
  echo "kernel-bitcode $1 is not valid" >&2
  exit 1
fi

CXXFLAGS="`$CLAMP_CONFIG --build --cxxflags`"
CO="-c -o"

TEMP_DIR=`mktemp -d`
BASENAME=`basename $2`
TEMP_NAME="$TEMP_DIR/$BASENAME"

# hip-kernel-assemble goes after hip-host-assemble, so attempt to link object from host
if [ -f $2 ]; then
  mv $2 $TEMP_DIR/$BASENAME.tmp.o
fi

$LLVM_DIS $1 -o $TEMP_NAME.ll
if [ $KMDUMPLLVM == "1" ]; then
  cp $TEMP_NAME.ll ./dump.kernel_input.ll
fi
$OPT -load $LIBPATH/LLVMDirectFuncCall.so -redirect < $TEMP_NAME.ll 2>$TEMP_NAME.kernel_redirect.ll >/dev/null
if [ $KMDUMPLLVM == "1" ]; then
  cp $TEMP_NAME.kernel_redirect.ll ./dump.kernel_redirect.ll
fi
if [[ -s $TEMP_NAME.kernel_redirect.ll ]]; then
  $OPT -load $LIBPATH/LLVMWrapperGen.so -gensrc < $TEMP_NAME.ll 2>$TEMP_NAME.camp.cpp >/dev/null
  if [ $KMDUMPLLVM == "1" ]; then
    cp $TEMP_NAME.camp.cpp ./dump.kernel_camp.cpp
  fi
  $LLVM_AS $TEMP_NAME.kernel_redirect.ll -o $TEMP_NAME.kernel_redirect.bc
  $CLANG $CXXFLAGS $TEMP_NAME.camp.cpp $CO $TEMP_NAME.camp.s -emit-llvm
  $CLANG $CXXFLAGS $TEMP_NAME.camp.cpp $CO $TEMP_NAME.camp.o
  objcopy -R .kernel $TEMP_NAME.camp.o
  $LLVM_LINK $TEMP_NAME.kernel_redirect.bc $TEMP_NAME.camp.s -o $TEMP_NAME.link.bc
  $CLAMP_ASM $TEMP_NAME.link.bc $TEMP_NAME.camp.o
else
  ln -s $1 $1.bc
  $CLAMP_ASM $1.bc $TEMP_NAME.camp.o
fi

if [ -f $TEMP_DIR/$BASENAME.tmp.o ]; then
  ld -r --allow-multiple-definition $TEMP_DIR/$BASENAME.tmp.o $TEMP_NAME.camp.o -o $2
else
  mv $TEMP_NAME.camp.o $2
fi

rm -f $TEMP_NAME.* $1.bc
rmdir $TEMP_DIR
